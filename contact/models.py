from django.db import models

# Create your models here.
class ContactMessage(models.Model):
    email = models.EmailField()
    phonenumber = models.CharField(max_length=300)
    requirement = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.email + " - " + self.message