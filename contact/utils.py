import random
import string

from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.text import slugify

from Faridabad_Plots.settings import EMAIL_HOST_USER



def send_get_in_touch_email_to_user(recipient_email):
    subject = "Thank You From Faridabad Plots for contacting us!"
    message = "Thank you for reaching out to us. We have received your message! We will get back to you soon!"
    sender_email = EMAIL_HOST_USER

    send_mail(subject, message, sender_email, [recipient_email], fail_silently=False)


def send_get_in_touch_email_to_host(contact_message):
    subject = "Faridabad Plots: New Message Received!"
    html_message = render_to_string("contact-us.html", {'contact_message': contact_message})
    sender_email = EMAIL_HOST_USER

    send_mail(subject, None, sender_email, [EMAIL_HOST_USER], fail_silently=False, html_message=html_message)
