from django.contrib import admin
from contact.models import ContactMessage

# Register your models here.
class ContactMessageAdmin(admin.ModelAdmin):
    class Meta:
        model = ContactMessage

    list_display = ['email', 'phonenumber', 'requirement']
    list_filter = ['timestamp']

admin.site.register(ContactMessage, ContactMessageAdmin)