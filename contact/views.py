from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView

from contact.models import ContactMessage

from contact.utils import (
    send_get_in_touch_email_to_host,
    send_get_in_touch_email_to_user,
)

# Create your views here.
class ContactMessagePostView(CreateView):
    model = ContactMessage
    fields = ['email', 'phonenumber', 'requirement']
    template_name = "contact-us.html"

    def post(self, request, *args, **kwargs):
        email = request.POST.get('email')
        contact_message = ContactMessage.objects.create(
            phonenumber=request.POST.get('phonenumber'),
            email=email,
            requirement=request.POST.get('requirement'),
        )

        send_get_in_touch_email_to_user(email)
        send_get_in_touch_email_to_host(contact_message)

        messages.success(request, "Thank you for contacting us! We will reach out to you soon!")
        return HttpResponseRedirect(reverse('index'))
        