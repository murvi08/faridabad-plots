from django.conf.urls import url

from contact.views import ContactMessagePostView

app_name = 'contact'

urlpatterns = [
    url(r'^', ContactMessagePostView.as_view(), name='post-contact-message'),
]